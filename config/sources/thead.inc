
INITRD_MODULES="tun:btrfs:xfs"

SERIAL_CONSOLE_SPEED=115200
SERIAL_CONSOLE=ttyS0

BOOT_LOADER_SOURCE="https://github.com/revyos/thead-u-boot.git"
BOOT_LOADER_BRANCH="th1520::"
BOOT_LOADER_DIR="u-boot-${SOCFAMILY}"

OPENSBI="true"
OPENSBI_PLATFORM="generic"
OPENSBI_BLOB="fw_dynamic.bin"

# disk sections table: dos or gpt
DISK_SECTION_TABLE="gpt"

# disk partition:
# PART:LABEL:FS:OFFSET:SIZE:TYPE
# ("1:boot:fat:empty:${PART_BOOT_SIZE}:EFI" "2:linuxroot:ext4:empty:empty:empty")
DISK_PARTITIONS=("1:boot:ext4:8192:${PART_BOOT_SIZE}:EFI" "2:linuxroot:ext4:empty:empty:empty")


BOOT_LOADER_BIN="u-boot-with-spl.bin"


case $KERNEL_SOURCE in
    legacy)
            #OPENSBI_SOURCE="https://github.com/revyos/thead-opensbi.git"
            #OPENSBI_DIR="opensbi-${SOCFAMILY}"
            #OPENSBI_BRANCH="lpi4a::"
            #KERNEL_BRANCH_VERSION="5.10"
            #LINUX_SOURCE="https://github.com/revyos/thead-kernel.git"
            #KERNEL_BRANCH="lpi4a::"

            OPENSBI_SOURCE="https://github.com/revyos/opensbi.git"
            OPENSBI_DIR="opensbi-${SOCFAMILY}"
            OPENSBI_BRANCH="th1520-v1.4::"
            KERNEL_BRANCH_VERSION="6.6"
            LINUX_SOURCE="https://github.com/revyos/th1520-linux-kernel.git"
            KERNEL_BRANCH="th1520-lts::"
            KERNEL_DIR="linux-$SOCFAMILY-$KERNEL_SOURCE-$KERNEL_BRANCH_VERSION"
    ;;
#    next)
#            KERNEL_BRANCH_VERSION="6.13"
#            KERNEL_BRANCH="linux-${KERNEL_BRANCH_VERSION/3/2}.y:tag:v${KERNEL_BRANCH_VERSION/3/2}"
#    ;;
esac




uboot_customization()
{
    :
}


create_uboot()
{
    pushd $SOURCE/$BOOT_LOADER_DIR >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    install -Dm644 $BOOT_LOADER_BIN $BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/$BOOT_LOADER_BIN >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    install -Dm644 $SOURCE/$OPENSBI_DIR/$OPENSBI_BLOB $BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/$OPENSBI_BLOB >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    popd >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
}


write_uboot()
{
    if [[ -f $BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/$BOOT_LOADER_BIN ]]; then
        message "" "write" "bootloader: $BOOT_LOADER_BIN"
        install -Dm644 $BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/$BOOT_LOADER_BIN $SOURCE/$IMAGE/boot/$BOOT_LOADER_BIN >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi

    if [[ -f $BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/$OPENSBI_BLOB ]]; then
        install -Dm644 $BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/$OPENSBI_BLOB $SOURCE/$IMAGE/boot/$OPENSBI_BLOB >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi

    if [[ -f $CWD/blobs/thead/boot/light_aon_fpga.bin ]]; then
        install -Dm644 $CWD/blobs/thead/boot/light_aon_fpga.bin $SOURCE/$IMAGE/boot/light_aon_fpga.bin >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi
    if [[ -f $CWD/blobs/thead/boot/light_c906_audio.bin ]]; then
        install -Dm644 $CWD/blobs/thead/boot/light_c906_audio.bin $SOURCE/$IMAGE/boot/light_c906_audio.bin >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi
    if [[ -f $CWD/blobs/thead/boot/str.bin ]]; then
        install -Dm644 $CWD/blobs/thead/boot/str.bin $SOURCE/$IMAGE/boot/str.bin >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi
}



