
INITRD_MODULES="tun:btrfs:xfs"

SERIAL_CONSOLE_SPEED=115200
SERIAL_CONSOLE=ttyS0

OVERLAY_PREFIX="starfive"


case $KERNEL_SOURCE in
    legacy)
            KERNEL_BRANCH_VERSION="6.6"
            LINUX_SOURCE="https://github.com/starfive-tech/linux"
            KERNEL_BRANCH="JH7110_VisionFive2_6.6.y_devel::"
            #KERNEL_BRANCH="JH7110_VisionFive2_6.1.y_devel::"
            #KERNEL_BRANCH="JH7110_VisionFive2_devel::"
            KERNEL_DIR="linux-$SOCFAMILY-$KERNEL_SOURCE-$KERNEL_BRANCH_VERSION"
    ;;
#    next)
#            LINUX_SOURCE="https://github.com/starfive-tech/linux"
#            KERNEL_BRANCH="JH7110_VisionFive2_upstream::"
#            KERNEL_DIR="linux-$SOCFAMILY-$KERNEL_SOURCE"
#    ;;
esac



uboot_prepare()
{
    if [[ ! -z $OPENSBI ]]; then
        ln -fs $SOURCE/$OPENSBI_DIR/build/platform/generic/firmware/$OPENSBI_BLOB -r $SOURCE/$OPENSBI_DIR/$OPENSBI_BLOB
    fi
}

create_uboot()
{
    pushd $SOURCE/$BOOT_LOADER_DIR >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1

    # u-boot-spl.bin.normal.out -> u-boot.bin
    if [[ -e spl/u-boot-spl.bin.normal.out ]]; then
        install -Dm644 spl/u-boot-spl.bin.normal.out $BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/u-boot.bin >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi
    if [[ -e u-boot.itb ]]; then
        install -Dm644 u-boot.itb $BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/u-boot.itb >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    fi

    popd >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
}


write_uboot()
{
    :
}

