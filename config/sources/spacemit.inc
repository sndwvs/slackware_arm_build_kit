
INITRD_MODULES="tun:btrfs:xfs"

SERIAL_CONSOLE_SPEED=115200
SERIAL_CONSOLE=ttyS0

BOOT_LOADER_SOURCE="https://github.com/BPI-SINOVOIP/pi-u-boot.git"
BOOT_LOADER_BRANCH="v2022.10-k1-v2.0::"
#BOOT_LOADER_SOURCE="https://gitee.com/bianbu-linux/uboot-2022.10.git"
#BOOT_LOADER_BRANCH="bl-v2.0.y::"
BOOT_LOADER_DIR="u-boot-${SOCFAMILY}"

OPENSBI_SOURCE="https://github.com/BPI-SINOVOIP/pi-opensbi.git"
OPENSBI_BRANCH="v1.3-k1::"
#OPENSBI_SOURCE="https://gitee.com/bianbu-linux/opensbi.git"
#OPENSBI_BRANCH="bl-v1.0.y::"
#OPENSBI_BRANCH="master:tag:v1.3"
OPENSBI_DIR="opensbi-${SOCFAMILY}"

OPENSBI="true"
OPENSBI_PLATFORM="generic"
OPENSBI_BLOB="fw_dynamic.bin"

# disk sections table: dos or gpt
DISK_SECTION_TABLE="gpt"

# disk partition:
# PART:LABEL:FS:OFFSET:SIZE:TYPE
# ("1:boot:fat:empty:${PART_BOOT_SIZE}:EFI" "2:linuxroot:ext4:empty:empty:empty")
DISK_PARTITIONS=("1:boot:ext4:32768:${PART_BOOT_SIZE}:EFI")
DISK_PARTITIONS+=("2:linuxroot:ext4:empty:empty:empty")


OVERLAY_PREFIX="spacemit"

BOOT_LOADER_BIN="FSBL.bin"


case $KERNEL_SOURCE in
    legacy)
            KERNEL_BRANCH_VERSION="6.6"
            #LINUX_SOURCE="https://gitee.com/bianbu-linux/linux-6.6.git"
            #KERNEL_BRANCH="bl-v2.0.y::"
            LINUX_SOURCE="https://github.com/jmontleon/linux-bianbu.git"
            KERNEL_BRANCH="linux-${KERNEL_BRANCH_VERSION}.y::"
            KERNEL_DIR="linux-$SOCFAMILY-$KERNEL_SOURCE-$KERNEL_BRANCH_VERSION"
    ;;
    next)
            KERNEL_BRANCH_VERSION="6.13"
            LINUX_SOURCE="https://github.com/jmontleon/linux-bianbu.git"
            KERNEL_BRANCH="linux-${KERNEL_BRANCH_VERSION}.y::"
            KERNEL_DIR="linux-$SOCFAMILY-$KERNEL_SOURCE-$KERNEL_BRANCH_VERSION"
    ;;
esac




uboot_customization()
{
    :
}


create_uboot()
{
    pushd $SOURCE/$BOOT_LOADER_DIR >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    install -Dm644 $CWD/blobs/spacemit/boot/u-boot-bpi-f3-header-sd-0k.img $BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/u-boot-bpi-f3-header-sd-0k.img >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    install -Dm644 $CWD/blobs/spacemit/boot/initramfs-generic.img $BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/initramfs-generic.img >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    install -Dm644 $BOOT_LOADER_BIN $BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/$BOOT_LOADER_BIN >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    install -Dm644 u-boot.itb $BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/u-boot.itb >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    install -Dm644 $SOURCE/$OPENSBI_DIR/$OPENSBI_BLOB $BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/$OPENSBI_BLOB >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    install -Dm644 $SOURCE/$OPENSBI_DIR/build/platform/generic/firmware/fw_dynamic.itb $BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/fw_dynamic.itb >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    install -Dm644 bootinfo_*.bin $BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/ >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
    popd >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
}


write_uboot()
{
    if [[ -f $BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/u-boot-bpi-f3-header-sd-0k.img ]]; then
        dd if=$BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/u-boot-bpi-f3-header-sd-0k.img of=$1 bs=512 status=noxfer >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        message "" "write" "bootloader: u-boot-bpi-f3-header-sd-0k.img"
    fi
    if [[ -f $BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/$BOOT_LOADER_BIN ]]; then
        dd if=$BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/$BOOT_LOADER_BIN of=$1 bs=512 seek=512 status=noxfer >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        message "" "write" "bootloader: $BOOT_LOADER_BIN"
    fi
    if [[ -f $BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/fw_dynamic.itb ]]; then
        dd if=$BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/fw_dynamic.itb of=$1 bs=512 seek=1280 status=noxfer >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        message "" "write" "bootloader: fw_dynamic.itb"
    fi
    if [[ -f $BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/u-boot.itb ]]; then
        dd if=$BUILD/$OUTPUT/$TOOLS/$BOARD_NAME/boot/u-boot.itb of=$1 bs=512 seek=2048 status=noxfer >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        message "" "write" "bootloader: u-boot.itb"
    fi

    if [[ -f $CWD/blobs/spacemit/boot/env_k1-x.txt ]]; then
        install -Dm644 $CWD/blobs/spacemit/boot/env_k1-x.txt $SOURCE/$IMAGE/boot/env_k1-x.txt >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        PRODUCT_NAME=$(echo ${DEVICE_TREE_BLOB} | sed 's:.*\/\(.*\).dtb:\1:g')
        sed -e "s:%PRODUCT_NAME%:${PRODUCT_NAME}:g" \
            -e "s:%DEVICE_TREE_BLOB%:${DEVICE_TREE_BLOB}:g" \
            -e "s:%SERIAL_CONSOLE%:${SERIAL_CONSOLE}:g" \
            -e "s:%SERIAL_CONSOLE_SPEED%:${SERIAL_CONSOLE_SPEED}:g" \
            -i $SOURCE/$IMAGE/boot/env_k1-x.txt
        message "" "install" "bootloader: env_k1-x.txt"
    fi

    # install on emmc
    #echo 0 | sudo tee /sys/block/mmcblk2boot0/force_ro
    #sudo dd if=bootinfo_emmc.bin of=/dev/mmcblk2boot0
    #sudo dd if=FSBL.bin of=/dev/mmcblk2boot0 seek=512 bs=1
}

kernel_customization() {

    if [[ $KERNEL_SOURCE == next ]]; then
        install -Dm644 $CWD/blobs/firmware/overall/esos.elf $SOURCE/$KERNEL_DIR/firmware/esos.elf >> $LOG 2>&1 || (message "err" "details" && exit 1) || exit 1
        message "" "kernel" "firmware customization"
    fi

}



